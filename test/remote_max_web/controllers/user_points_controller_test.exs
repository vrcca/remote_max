defmodule RemoteMaxWeb.UserPointsControllerTest do
  use RemoteMaxWeb.ConnCase, async: true

  alias RemoteMax.Repo

  @update_delay 500

  setup do
    users_pid = start_supervised!({RemoteMax.PointsTracker, %{update_delay: @update_delay}})
    Ecto.Adapters.SQL.Sandbox.allow(Repo, self(), users_pid)
  end

  describe "GET /" do
    test "it returns 200 with users and timestamp", %{conn: conn} do
      body = user_points_response(conn)
      assert %{"users" => _users, "timestamp" => _} = body
    end

    test "returns a maximum of 2 users", %{conn: conn} do
      Repo.insert_all("users", Enum.map(1..3, fn _ -> build_user(%{points: 101}) end))
      body = user_points_response(conn)
      assert Enum.count(body["users"]) == 2
    end

    test "each user has positive id and points", %{conn: conn} do
      Repo.insert_all("users", Enum.map(1..2, fn _ -> build_user(%{points: 101}) end))
      body = user_points_response(conn)
      assert %{"users" => users} = body

      for user <- users do
        assert %{"id" => id, "points" => points} = user
        assert id >= 0
        assert points >= 0
      end
    end

    test "timestamp is nil on the first call, filled and updated on consecutive calls", %{
      conn: conn
    } do
      body = user_points_response(conn)
      assert body["timestamp"] == nil

      body = user_points_response(conn)
      timestamp = body["timestamp"]
      assert timestamp != nil

      body = user_points_response(conn)
      new_timestamp = body["timestamp"]
      assert new_timestamp != nil
      assert timestamp != new_timestamp
    end

    test "after a while, the list of users and points should be updated", %{conn: conn} do
      Repo.insert_all("users", Enum.map(1..10, fn _ -> build_user(%{points: 0}) end))
      previous_data = user_points_response(conn)

      change = fn ->
        user_points_response(conn)["users"] != previous_data["users"]
      end

      has_changed? = wait(change)
      assert has_changed? == true
    end
  end

  defp wait(condition) do
    Enum.reduce_while(1..100, false, fn _x, _acc ->
      if condition.() do
        {:halt, true}
      else
        :timer.sleep(100)
        {:cont, false}
      end
    end)
  end

  defp user_points_response(conn) do
    conn = get(conn, Routes.user_points_path(conn, :index))
    json_response(conn, 200)
  end

  defp build_user(user) do
    user
    |> Map.put(:inserted_at, DateTime.utc_now())
    |> Map.put(:updated_at, DateTime.utc_now())
  end
end
