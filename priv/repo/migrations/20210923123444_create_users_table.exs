defmodule RemoteMax.Repo.Migrations.CreateUsersTable do
  use Ecto.Migration

  def change do
    create table("users") do
      add :points, :integer, default: 0
      timestamps()
    end

    flush()

    # We always update 100% of tuples. So adding more space upfront to avoid vacuuming and IO penalties.
    # See https://www.postgresql.org/docs/13/sql-createtable.html#RELOPTION-FILLFACTOR
    execute("ALTER TABLE users SET (fillfactor = 50)")
  end
end
