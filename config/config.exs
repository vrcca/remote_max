# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :remote_max,
  ecto_repos: [RemoteMax.Repo]

# Configures the endpoint
config :remote_max, RemoteMaxWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "wM6AErWdagnRXMGBAxX6Lq2Ki0zoN9k+BP39yy5Ou2VSii1I7Mv1Id95Ut50N/Je",
  render_errors: [view: RemoteMaxWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: RemoteMax.PubSub,
  live_view: [signing_salt: "AeoWOPw9"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
