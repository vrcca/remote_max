defmodule RemoteMax.Repo do
  use Ecto.Repo,
    otp_app: :remote_max,
    adapter: Ecto.Adapters.Postgres
end
