defmodule RemoteMax.Users do
  require Logger
  import Ecto.Query, only: [from: 2]
  alias RemoteMax.Repo

  def list(max_number) do
    query =
      from u in "users",
        select: %{id: u.id, points: u.points},
        where: u.points > ^max_number,
        limit: 2

    Repo.all(query)
  end

  def update_points!() do
    Logger.info(message: "Update points for all users...")

    query =
      from u in "users",
        update: [
          set: [
            points: fragment("floor(random() * 100 + 1)::int"),
            updated_at: ^DateTime.utc_now()
          ]
        ]

    {updated, _} = Repo.update_all(query, [])
    Logger.info(message: "All points were updated.", updated: updated)
  end
end
