# RemoteMax

This service shows 2 or less users within a max number of points threshold.
We have 1 million users. Every 1 minute it updates each user's points and a max number is randomly selected.

## Dependencies

- Elixir 1.7+
- Docker (Optional)
- Make (Optional)
- Postgres 13 on port 5432 (default user and pass: postgres)

## How to run

Make sure you have Postgres 13 running on port `5432`. Then, open your terminal and run:

``` sh
mix deps.get
mix ecto.setup
mix phx.server
```

Optionally, you can just run: 

``` sh
make run
```

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.


## Assumptions

- Given that we need to update 1 million tuples every minute, I optimized the table for updates instead of searching. My tests showed the search was fast enough (<100ms).
- We want to avoid vacuuimg and rebuilding indexes every minute. So I set user's table `fillfactor` to 50% for HOT updates and did not add an index to `points` column on purpose. This way updates are pretty fast (5s max) compared to no fillfactor (>15s).
- If the GenServer crashes, the `timestamp` will be lost.
- If in the future the ordering of the results become important, we may need adding indexes to `points` column.
- Since we only have 1 million tuples, I decided to leave the update to Postgres. So no batching was necessary.
- I didn't create any schemas because I wanted to keep it simple, and the business logic is data focused.
- It's ok to update `max_number` before finishing updating the user's points
- There was no need for a Dockerfile or a release setup.
- `handle_call` is expected to be quick enough. If anything, we can always `GenServer.reply` async
