.PHONY := test start-db stop-db run build format migrate

build:
	mix do deps.get, compile

test:
	mix test

start-db:
	docker-compose up -d

stop-db:
	docker-compose down

run: start-db build migrate
	mix phx.server

migrate:
	mix ecto.setup
