defmodule RemoteMaxWeb.UserPointsController do
  use RemoteMaxWeb, :controller

  def index(conn, _params) do
    data = RemoteMax.PointsTracker.list()
    json(conn, data)
  end
end
