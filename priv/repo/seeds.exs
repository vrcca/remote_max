# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     RemoteMax.Repo.insert!(%RemoteMax.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias RemoteMax.Repo

unless Repo.exists?("users") do
  Repo.query!("""
  INSERT INTO users (points, inserted_at, updated_at)
  SELECT 0, LOCALTIMESTAMP, LOCALTIMESTAMP FROM generate_series(1, 1000000)
  """)
end
