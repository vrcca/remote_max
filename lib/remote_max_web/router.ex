defmodule RemoteMaxWeb.Router do
  use RemoteMaxWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", RemoteMaxWeb do
    pipe_through :api

    get "/", UserPointsController, :index
  end
end
