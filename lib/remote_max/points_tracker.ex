defmodule RemoteMax.PointsTracker do
  use GenServer
  alias RemoteMax.Users

  @default_delay :timer.seconds(60)

  def start_link(args) do
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  def init(args) do
    delay = args[:update_delay] || @default_delay
    initial_state = %{timestamp: nil, max_number: random_number()}
    schedule_update(delay)
    {:ok, initial_state}
  end

  defp random_number(), do: Enum.random(1..100)

  defp schedule_update(delay), do: Process.send_after(self(), {:update_points, delay}, delay)

  def handle_call(:list, _from, state) do
    users = Users.list(state.max_number)
    data = %{users: users, timestamp: state.timestamp}
    {:reply, data, %{state | timestamp: DateTime.utc_now()}}
  end

  def handle_info({:update_points, delay}, state) do
    Task.start_link(&Users.update_points!/0)
    new_state = %{state | max_number: random_number()}
    schedule_update(delay)
    {:noreply, new_state}
  end

  def list(), do: GenServer.call(__MODULE__, :list)
end
